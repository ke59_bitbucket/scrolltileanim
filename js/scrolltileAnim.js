function ScrollTileAnim(opts) {
	this.setVar(opts);
	this.setTile();
	this.setEvent();
}

//オプション設定
ScrollTileAnim.prototype.setVar = function(opts) {
	var self = this;
	this.opts = { //defult option
		tiles: 5, //タイルの数
		bg: '#fff', //タイルに設定する背景色
		selector: '.selector', //実行セレクタ
		pos: 500, // アニメーション実行位置調整
		delay: 25 // アニメーション遅延時間
	};

	//指定オプション設定
	Object.keys(this.opts).forEach(function(k) {
		if (typeof opts[k] !== 'undefined' && typeof self.opts[k] !== 'undefined') self.opts[k] = opts[k];
	});
	this.opts.widthHeight = (Math.floor(100 / Number(this.opts.tiles) * 100) / 100) + '%';
	this.opts.offset = this.opts.selector.offsetTop - this.opts.pos;
};

//
ScrollTileAnim.prototype.setTile = function() {
	var outer = document.createElement('div');
	outer.setAttribute('class', 'scrollTileAnim-outer');

	for (var i = 0; i < this.opts.tiles; i++) {
		for (var j = 0; j < this.opts.tiles; j++) {
			let inner = document.createElement('div');
			inner.setAttribute('class', 'scrollTileAnim-inner');
			inner.style.width = this.opts.widthHeight;
			inner.style.height = this.opts.widthHeight;
			inner.style.backgroundColor = this.opts.bg;
			// inner.style.animationName = 'fade';
			inner.style.animationDuration = '1s';
			inner.style.animationDirection = 'normal';
			inner.style.animationTimingFunction = 'ease-out';
			inner.style.animationFillMode = 'forwards';
			inner.style.animationDelay = (j * (i / this.opts.delay)) + 's';
			outer.appendChild(inner);
		}
	}

	this.opts.selector.insertBefore(outer, this.opts.selector.firstChild);
};

ScrollTileAnim.prototype.setEvent = function() {
	var self = this;
	['scroll', 'load'].forEach(function(event, i) {
		window.addEventListener(event, function() {
			if (window.pageYOffset > self.opts.offset) {
				self.opts.selector.classList.add('on');
				Array.prototype.forEach.call(self.opts.selector.children[0].children,function(node,i){
					node.style.animationName = 'fade';
				});
				// (.scrollTileAnim .scrollTileAnim-outer).style.animationName = 'fade';

			}
		});
	});
}