# ScrollTileAnim 
タイル状に設定された単色背景が、フェードアウトするJavaScriptライブラリ

## 設定方法
```
new ScrollTileAnim({  
	tiles: 5,//タイルの数  
	bg: '#fff',//タイルに設定する背景色  
	selector:'.selector',//実行セレクタ  
	pos: 500, // アニメーション実行位置調整  
	delay: 25 // アニメーション遅延時間  
});
```